#include "mmio.h"

#define X 0x2000
#define Y 0x2004
#define Z 0x2008

unsigned int adder_ref(unsigned int x, unsigned int y)
{
	unsigned int z = x + y;
	return z;
}

int main(void)
{
	unsigned int result, ref, x = 3, y = 2;
	//wait for the peripheral to be ready
	//while( () == 0);

	//write inputs to the peripheral
	reg_write32(X,x);
	reg_write32(Y,y);

	//wait for the peripheral to complete
	//while( () == 0);

	//read outputs from the peripheral
	result = reg_read32(Z);

	//check
	ref = adder_ref(x, y);

	if(result != ref)
	{
		printf("The result %d did not match the reference value %d", result, ref);
		return 1;
	}
	return 0;
}