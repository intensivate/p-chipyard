package chipyard.example

import chisel3._
import chisel3.util._
import freechips.rocketchip.subsystem.{BaseSubsystem, CacheBlockBytes}
import freechips.rocketchip.config.{Parameters, Field, Config}
import freechips.rocketchip.diplomacy.{LazyModule, LazyModuleImp, IdRange}
import testchipip.TLHelper

//1.
case class JustWriteConfig(
	base: BigInt,
	size: BigInt)
//spisak parametara koje ce moj widget (dodatak? projekat? podsistem) koristiti
//parametre pakujemo u CONFIG (ili PARAMS)
//pripada Scala-land, dakle tipovi podataka su Scala tipovi podataka

//2.
case object JustWriteKey extends Field[Option[JustWriteConfig]](None)
// KEY specificira parametar kojim se kontrolise vidzet / widget <== dodatak? addon? Prosirenje?...
//	a realizuje se kao Option tip, default = None (sto znaci no-op ako korisnik ne zahtijeva)
//  KEY == sub-project <== dajemo mu ovaj Config iznad
//  pristup stavkama koje se cuvaju u tom kljucu (KEY) moguc je
//	pod uslovom da se implicit p: Parameters prosljedjuje na odgovarajuci nacin
//	npr: p(JustWriteKey).get.base <<=== vraca ono sto je gore u JustWriteConfig.base
//  naravno, ovo ce da radi ako Key vise ne bude None

//3. outer twin
class JustWrite(implicit p: Parameters) extends LazyModule { 
  val node = TLHelper.makeClientNode(name = "just-write", sourceId = IdRange(0, 1))

  lazy val module = new JustWriteModuleImp(this)
}
//	pravim widget koji ce da radi kao TL master, tj.
//	moze sam da upise vrijednost u memoriju, bez interakcije sa jezgrom
// 		makeClientNode pravi TL klijenta (node)
//		tog klijenta spajamo na frontbus (fbus) <== nize, tacka 6
//
//		o klijentu: https://chipyard.readthedocs.io/en/latest/TileLink-Diplomacy-Reference/NodeTypes.html#client-node
//
//		klijenti su moduli koji iniciraju TL transakcije tako sto posalju zahtjev na A kanalu i 
//		citaju odgovor na D kanalu TL protokola (u osnovnoj varijanti, inace moze i TL sa C,B,E kanalima)
//		L1 kes i DMA uredjaji imaju takve klijente kao TL cvorove
//			"name" je identifikator cvora u Diplomacy grafu <== neophodan za TLClientParameters
//			"sourceId" specificira opseg identifikatora poruka koje klijent koristi, tj.
//						to je broj transakcija koje klijent moze istovremeno da posalje, pri cemu
//						opseg (0,1) znaci da moze da posalje samo jednu, a (0,4) moze cetiri razlicite
//		pozivom node.out dobijamo spisak parova bundle/edge (TLHelper ima samo jedan taj par)
//(KOJI bundle???)
//			a EDGE je objekat u vezi sa NODE, koji predstavlja kolekciju metoda (funkcija) za komunikaciju na TL
//				NPR: GET => zahtjeva podatke iz memorije preko TLBundleA kanala
//						PUT => pise u memoriju kanalom A
//						ima ih jos: https://chipyard.readthedocs.io/en/latest/TileLink-Diplomacy-Reference/EdgeFunctions.html#tilelink-edge-object-methods
//			
//		tl bundle je Chisel bundle koji se spaja na IO bundle modula koji pravimo
//			sastoji se od dva druga (decoupled) bundla od kojih svaki odgovara po jednom TL kanalu
//				ETO TU treba spojiti nas hardver da bi komunicirao preko TL

//4. inner twin - implementacija, module
class JustWriteModuleImp(outer: JustWrite) extends LazyModuleImp(outer) {
  val config = p(JustWriteKey).get //<== preuzima CONFIG/PARAM

  val (tl, edge) = outer.node.out(0) // ovo je ono node.out, kao vraca par bundle/edge
  // otprilike ovo tl je bundle (tj. kolekcija) bundlova, tj. kolekcija kanala a svaki taj kanal
  // bi valjda bio kolekcija interfejsa (portova) za taj kanal
  // edge je taj objekat koji sadrzi  te funkcije koje su nam od interesa za rad za tl
  

  val addrBits = edge.bundle.addressBits // ove dvije nije bas jasno sta rade... 
  // bice da imaju veze sa samim rocket-chipom, tj. njegovom konfiguracijom, da bismo
  // ovdje znali cemu smijemo da pridjemo
  val blockBytes = p(CacheBlockBytes)

  require(config.size % blockBytes == 0)

  // je li OVAJ FSM neophodan? Bice da jeste, kako bi se moglo ispravno signalizirati na ove ready/valid
  val s_init :: s_write :: s_resp :: s_done :: Nil = Enum(4)
  val state = RegInit(s_init)

  val addr = Reg(UInt(addrBits.W)) // ZASTO OVO nije nula na reset, tj. RegInit?
  val bytesLeft = Reg(UInt(log2Ceil(config.size+1).W))

  tl.a.valid := state === s_write
  tl.a.bits := edge.Put( // AHA, evo ga PUT iz EDGE https://chipyard.readthedocs.io/en/latest/TileLink-Diplomacy-Reference/EdgeFunctions.html#put
    fromSource = 0.U, 					// sourceId za ovu transakciju
    toAddress = addr, 					// adresa na koju se pise
    lgSize = log2Ceil(blockBytes).U,	// ld (logaritam po bazi 2) broja bajta koji treba da se upisu
    data = 230.U)._2 						// podaci za upis, ._2 selektuje drugi po redu element u tuplu
  											//		a PUT, inace, vraca tuple: 
  											// 			prvi element je Bool da li je operacija legalna ili ne
  											//			drugi element je A kanal bundle
  tl.d.ready := state === s_resp

  when (state === s_init) {
    addr := config.base.U
    bytesLeft := config.size.U
    state := s_write
  }

// edge.done vraca BOOLean TRUE ako je trenutni beat (? byte??) posljednji
//							FALSE ako nije posljednji
  when (edge.done(tl.a)) {
    addr := addr + blockBytes.U
    bytesLeft := bytesLeft - blockBytes.U
    state := s_resp
  }

  when (tl.d.fire()) { // FIRE je isto sto i ready & valid, tj 1 je kada su i ready i valid 1
    state := Mux(bytesLeft === 0.U, s_done, s_write)
  }
}

//5. CanHave, u sustini ovdje preusmjeravamo Key da ne bude None
//		nego da pokazuje na ovaj nas widget
//		spaja widget na TL fbus, tj. Diplomacy mrezu
trait CanHavePeripheryJustWrite { this: BaseSubsystem =>
  implicit val p: Parameters

  p(JustWriteKey) .map { k =>
    val justWrite = LazyModule(new JustWrite()(p)) //lazy instanca spoljnjeg blizanca
    fbus.fromPort(Some("just-write"))() := justWrite.node // spajanje na fbus
  }
}

//6. Implementation trait ce natjerati DigitalTop da instancira jos jedan port
//		i spoji se na ovaj widget
class WithJustWrite(base: BigInt, size: BigInt) extends Config((site, here, up) => {
  case JustWriteKey => Some(JustWriteConfig(base, size))
})
// ovo (6) se jos zove i CONFIG FRAGMENT. Kada se ovaj fragment doda  u Config, 
//		JustWriteKey se preusmjerava na JustWriteConfig, cime se obezbjedjuje
//		da ostali trejtovi instanciraju jebeni vidzet i ispravno se spoje s njim

//5 i 6 su neka vrsta omotaca, opet jedan je lazy drugi je impl, oko outer/inner widzeta, tj. tacaka 3 i 4.


// edge
//		tl
//				decoupled
//						kanali <== tako nekako bi bila hijerarhija