package chipyard.example

import chisel3._
import chisel3.util._
import chisel3.experimental.{IntParam, BaseModule}
import freechips.rocketchip.amba.axi4._
import freechips.rocketchip.subsystem.BaseSubsystem
import freechips.rocketchip.config.{Parameters, Field, Config}
import freechips.rocketchip.diplomacy._
import freechips.rocketchip.regmapper.{HasRegMap, RegField}
import freechips.rocketchip.tilelink._
import freechips.rocketchip.util.UIntIsOneOf

// prva stvar, case class sa parametrima
case class JustReadParams(
	address: BigInt = 0x2000, //<-- treba offset da bude MMIO
	width: Int = 32
	)

// u vezi sa ovim prvim, ali nije jasno o cemu se radi
case object JustReadKey extends Field[Option[JustReadParams]](None)

// druga stvar pod a, bundle <== interfejsi
class JustReadIO(val w:Int) extends Bundle {
	val toBeRead = Output(UInt(w.W))
}

// druga stvar pod b, bundle trait <== instanca onih interfejsa pod a
// da se koriste kada se pravi modul
trait HasJustReadIO extends BaseModule {
	val w : Int
	val io = IO(new JustReadIO(w))
}

// druga stvar pod c, bundle za top, jer imamo i onaj regmap
trait JustReadTopIO extends Bundle {
	val isReady = Output(Bool())
}

//treca stvar pod a module implementation
class JustReadMMIOChiselModule(val w: Int) extends Module
	with HasJustReadIO
{
	// ZASTO ovi inace (GCD.scala i Adder.scala) imaju CLOCK i RESET?
			// Zar to ne dolazi podrazumijevano sa io i Module?
			
	val a = RegInit(230.U(w.W))//from Celio: https://github.com/ccelio/chisel-style-guide#registers

	io.toBeRead := a
}

//treca stvar pod b povezuje modul iznad na registre, da bude MMIO
trait JustReadModule extends HasRegMap
{
	val io: JustReadTopIO
	implicit val p: Parameters
	def params: JustReadParams

	val isJustRead = Reg(UInt(params.width.W))

	val impl = Module(new JustReadMMIOChiselModule(params.width))

	isJustRead := impl.io.toBeRead

	regmap(
		0x00 -> Seq(
			RegField.r(params.width, isJustRead)) // read-only registar u
					// koji se upisuje theValue, preko toBeRead i isJustRead
	)
}

//cetvrta stvar jeste konstrukcija konacnog omotaca koji 
//koji omogucava povezivanje na TileLink
class JustReadTL(params: JustReadParams, beatBytes: Int)(implicit p: Parameters)
	extends TLRegisterRouter(
		params.address, "justread", Seq("apaj,justreadit"),
		beatBytes = beatBytes)(
			new TLRegBundle(params, _) with JustReadTopIO)(
				//O bundle constructor, which we create by
				//extending TLRegBundle with our bundle trait
				new TLRegModule(params, _, _) with JustReadModule)
				//module constructor, which we create by extends TLRegModule 
				//with our module trait.

//peta stvar je povezivanje ovog TileLink omotaca sa ostatkom SoC
// to radimo koristeci CAKE PATTERN <===> placing code inside traits

// peta stvar pod a; LazyModule je setup koji se mora izvrsiti 
// prije elaboracije hardvera
// U ovom slucaju, samo povezujemo TileLink node na MMIO crossbar
trait CanHavePeripheryJustRead { this: BaseSubsystem =>
	private val portName = "justreadPortName"

	val justread = p(JustReadKey) match { 	//STA JE OVO?
		case Some(params) => { 				// KOJI KURAC JE SOME?
			val justread = LazyModule(new JustReadTL(params, pbus.beatBytes)(p))
			pbus.toVariableWidthSlave(Some(portName)) { justread.node } // ovo ".node" je TL NODE
			// i to je ono sto povezujemo na RocketChip TL magistralu
			Some(justread)
		}
		case None => None
	}
}

// peta stvar pod b; sada implementacija, tj. inner twin
trait CanHavePeripheryJustReadModuleImp extends LazyModuleImp {
	val outer: CanHavePeripheryJustRead
}

//sesta stvar, dodaj u DigitalTop:
//with chipyard.example.CanHavePeripheryJustRead
//i, malo nize, u drugom bloku:
//with chipyard.example.CanHavePeripheryJustReadModuleImp


//sedma stvar, napravi konfiguraciju u:
// generators/chipyard/src/main/scala/config/RocketConfigs.scala
// tako da koristi ovo:
class WithJustRead() extends Config((site,here,up) => {
	case JustReadKey => Some(JustReadParams())
})