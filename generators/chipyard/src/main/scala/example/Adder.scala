package chipyard.example

import chisel3._
import chisel3.util._
import chisel3.experimental.{IntParam, BaseModule}
import freechips.rocketchip.amba.axi4._
import freechips.rocketchip.subsystem.BaseSubsystem
import freechips.rocketchip.config.{Parameters, Field, Config}
import freechips.rocketchip.diplomacy._
import freechips.rocketchip.regmapper.{HasRegMap, RegField}
import freechips.rocketchip.tilelink._
import freechips.rocketchip.util.UIntIsOneOf

case class AdderParams(
  address: BigInt = 0x2000,
  width: Int = 32,
  useAXI4: Boolean = false)

case object AdderKey extends Field[Option[AdderParams]](None)

class AdderChiselModule(val w: Int) extends Module{
   val io = IO(new Bundle {
   val clock = Input(Clock())
   val reset = Input(Bool())
   //val input_ready = Output(Bool())
   //val input_valid = Input(Bool())
   val x = Input(UInt(w.W))
   val y = Input(UInt(w.W))
   //val output_ready = Input(Bool())
   val out = Output(UInt(w.W))
   })

   val out_reg = RegNext(io.x + io.y)
   io.out := out_reg
}

trait AdderModule extends HasRegMap {
 implicit val p: Parameters
 def params: AdderParams
 val clock: Clock
 val reset: Reset

 val x = Wire(UInt(params.width.W))
 val y = Wire(UInt(params.width.W))//new DecoupledIO(UInt(params.width.W)))
 val out = Wire(UInt(params.width.W))//new DecoupledIO(UInt(params.width.W)))

 val impl = Module(new AdderChiselModule(params.width))

 impl.io.clock := clock
 impl.io.reset := reset.asBool
 impl.io.x := x
 impl.io.y := y//.bits
 //impl.io.input_valid := y.valid
 //y.ready := impl.io.input_ready

 out := impl.io.out//.bits := impl.io.out
 //out.valid := y.valid
 //impl.io.output_ready := out.ready

  regmap(
    0x00 -> Seq(
      RegField.w(params.width, x)), // a plain, write-only register
    0x04 -> Seq(
      RegField.w(params.width, y)), // write-only, y.valid is set on write
    0x08 -> Seq(
      RegField.r(params.width, out))) // read-only, out.ready is set on read
}

class AdderTL(params: AdderParams, beatBytes: Int)(implicit p: Parameters)
  extends TLRegisterRouter(
    params.address, "adder", Seq("ucbbar,adder"),
    beatBytes = beatBytes)(
      new TLRegBundle(params, _))(
      new TLRegModule(params, _, _) with AdderModule)

class AdderAXI4(params: AdderParams, beatBytes: Int)(implicit p: Parameters)
  extends AXI4RegisterRouter(
    params.address,
    beatBytes=beatBytes)(
      new AXI4RegBundle(params, _) )(
      new AXI4RegModule(params, _, _) with AdderModule)

trait CanHavePeripheryAdder { this: BaseSubsystem =>
  private val portName = "adder"

  // Only build if we are using the TL (nonAXI4) version
  val adder = p(AdderKey) match {
    case Some(params) => {
      if (params.useAXI4) {
        val adder = LazyModule(new AdderAXI4(params, pbus.beatBytes)(p))
        pbus.toSlave(Some(portName)) {
          adder.node :=
          AXI4Buffer () :=
          TLToAXI4 () :=
          // toVariableWidthSlave doesn't use holdFirstDeny, which TLToAXI4() needsx
          TLFragmenter(pbus.beatBytes, pbus.blockBytes, holdFirstDeny = true)
        }
        Some(adder)
      } else {
        val adder = LazyModule(new AdderTL(params, pbus.beatBytes)(p))
        pbus.toVariableWidthSlave(Some(portName)) { adder.node }
        Some(adder)
      }
    }
    case None => None
  }
}

trait CanHavePeripheryAdderModuleImp extends LazyModuleImp {
  val outer: CanHavePeripheryAdder
}



class WithAdder(useAXI4: Boolean) extends Config((site, here, up) => {
  case AdderKey => Some(AdderParams(useAXI4 = useAXI4))
})
